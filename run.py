from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/ping", methods=['GET'])
def ping():
    return jsonify(success=True,response="pong!"), 200

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
